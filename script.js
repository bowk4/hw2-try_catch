const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
class Main {
    constructor(author, name, price) {
        this.author = author;
        this.name = name;
        this.price = price;
    }

    isValid() {
       return this.author && this.name && this.price ;
    }
}

function createList() {
    const callId = document.getElementById("root");
    const ulList = document.createElement("ul")

    books.forEach(bookLs => {
        try {
            const main = new Main(bookLs.author, bookLs.name, bookLs.price);

            if (main.isValid()) {
                const li = document.createElement("li");
                li.textContent = `${main.author} - ${main.name} - ${main.price}`;
                ulList.appendChild(li);
            } else {
                throw new Error(`Invalid number of properties in the book object: ${JSON.stringify(bookLs)}`);
            }
        } catch (error) {
            console.error(error.message);
        }
    })
    callId.appendChild(ulList);
}

createList();